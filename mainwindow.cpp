#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopServices>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("URL Opener");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_OK_clicked()
{
    QStringList liste_url = ui->plainTextEdit->toPlainText().split('\n', QString::SkipEmptyParts);

    QStringList url_error_list;

    if(liste_url.size() > 0)
    {
        for(int i = 0; i < liste_url.size(); i++)
        {
            if (liste_url[i].size() > 0)
            {
                if(!QDesktopServices::openUrl(QUrl(liste_url[i])))
                {
                    url_error_list.append(liste_url[i]);
                }
            }
        }

        if(url_error_list.size() > 0)
        {
            QString error_message = "Une erreur a été rencontrée pour les liens suivants :<ul>";

            for(int i = 0; i < url_error_list.size(); i++)
            {
                error_message += "<li>" + url_error_list[i] + "</li>";
            }

            error_message += "</ul>";

            QMessageBox::warning(this, "Erreur", error_message);
        }
    }
}

void MainWindow::on_pushButton_Clean_clicked()
{
    ui->plainTextEdit->setPlainText("");
}
